
var usuarios = require('./usuarios.json')
var usuarios2 = require('./usuarios-2.json')

app.get('/apitechu/v1', function(req, res)
{
  //console.log(req)
  res.send({"mensaje": "Bienvenido a mi API"})
})

app.get('/apitechu/v1/usuarios', function(req, res)
{
  //res.send(usuarios)
  res.send(usuarios2)
})

app.post('/apitechu/v1/usuarios', function(req, res)
{
  var nuevo = {"first_name": req.headers.first_name, "country": req.headers.country}
  usuarios.push(nuevo)
  console.log(req.headers)
  const datos = JSON.stringify(usuarios)

  fs.writeFile("./usuarios.json", datos, "utf8", function(err) {
    if (err) {
      console.log(err)
    }
    else {
      console.log("Fichero guardado")
    }
    })
  res.send("ALTA OK")
})

app.delete('/apitechu/v1/usuarios/:id', function(req, res)
{
  usuarios.splice(req.params.id-1, 1)
  res.send("Usuario borrado")
})

app.post('/apitechu/v1/monstruo/:p1/:p2', function(req, res) {
  console.log("Lista de parámetros")
  console.log(req.params)
  console.log("Query strings")
  console.log(req.query)
  console.log("Headers")
  console.log(req.headers)
  console.log("Body")
  console.log(req.body)
})

app.post('/apitechu/v2/usuarios', function(req, res)
{
  //var nuevo = {"first_name": req.headers.first_name, "country": req.headers.country}
  var nuevo = req.body
  usuarios.push(nuevo)
  //console.log(req.headers)
  const datos = JSON.stringify(usuarios)

  fs.writeFile("./usuarios.json", datos, "utf8", function(err) {
    if (err) {
      console.log(err)
    }
    else {
      console.log("Fichero guardado")
    }
    })
  res.send("ALTA OK")
})

app.post('/apitechu/v1/login', function(req, res)
{
  var email = req.headers.email
  var password = req.headers.password
  var idusuario = 0
  var idarray = 0
  console.log("Headers")
  console.log(req.headers)

  for (var i = 0; i < usuarios2.length; i++) {
    if (usuarios2[i].email==email && usuarios2[i].password==password) {
      idusuario=usuarios2[i].id
      usuarios2[i].logged=true
      idarray=i
      break;
    }
  }
  if (idusuario!=0) {
    res.send({"mensaje":"Login successful", "idusuario":idusuario, "idarray":idarray})
  } else {
    res.send({"encontrado":"no"})
  }
})

app.get('/apitechu/v1/usuarios2', function(req, res) {
  res.send(usuarios2)
})

app.post('/apitechu/v1/logout', function(req, res) {
  var idarray = req.headers.idusuario - 1
  if (usuarios2[idarray].logged) {
    usuarios2[req.headers.idusuario].logged = false
    res.send({"mensaje":"Logout successful", "idusuario":req.headers.idusuario})
  } else {
    res.send({"encontrado":"no"})
  }
})
