// Set other app paths
// require('./config/global-paths');
// Set config variables
//var global.config = require('./config');

const APP_HTTPS = false

var fs = require('fs') // módulo fs
var https = require('https') // módulo https
var express = require('express') // módulo express
var bodyParser = require('body-parser') // módulo body-parse
var requestJson = require('request-json') // módulo request-json

// Configuración del servidor
var port = process.env.PORT || 3000 // Definimos el puerto
var app = express()

if (APP_HTTPS) {
  https.createServer({
    key: fs.readFileSync('./certificate/borradorproyecto.key'),
    cert: fs.readFileSync('./certificate/borradorproyecto.crt')
  }, app).listen(port, function() {
    console.log("Mi servidor HTTPS atendiendo en el puerto " + port + "...");
  });
} else {
  app.listen(port)
  console.log("API escuchando en el puerto " + port)
}

app.use(bodyParser.json()) // Indicamos que la app lea el body con formato json

var urlMlabRaiz = "https://api.mlab.com/api/1/databases/techuaoa/collections"
var apiKey = "apiKey=6huaXLi3YdHe13R9DPEj8zVTwS8h_C7Z"

var usuarios = require('./usuarios.json')
var usuarios2 = require('./usuarios-2.json')

app.get('/apitechu/v1', function(req, res) {
  // console.log(res)
  res.status(200).send({"mensaje": "Bienvenido a mi API"})
})

app.get('/apitechu/v1/usuarios', function(req, res) {
  // console.log(res)
  res.status(200).send(usuarios2)
})

// Llamada mlabs; nueva versión api para atacar una BD NoSQL (Mongo) a través del API de mlab
app.get('/apitechu/v5/usuarios', function(req, res) {
  var clienteMlab = requestJson.createClient(urlMlabRaiz + "/usuarios?" + apiKey)
  clienteMlab.get('', function(err, resM, body) {
      if (!err) {
        res.status(200).send(body)
      } else {
        res.status(404).send({"mensaje":"Error"})
      }
  })
})

// Llamada mlabs; nueva versión api para atacar una BD NoSQL (Mongo) a través del API de mlab
app.get('/apitechu/v5/usuarios/:id', function(req, res) {
  var id = req.params.id
  var query = 'q={"id":' + id + '}&f={"first_name":1, "last_name":1, "_id":0}'
  var clienteMlab = requestJson.createClient(urlMlabRaiz + "/usuarios?" + query + "&" + apiKey) // el apiKey siempre tiene que ir al final ... parece ser que node no entiende qué es si no va al final
  clienteMlab.get('', function(err, resM, body) {
      if (!err) {
        if (body.length > 0) {
          //var user = []
          //user.firstname = {"first_name":body[0].first_name}
          //user.lastname = {"lastname":body[0].last_name}
          res.status(200).send({"first_name":body[0].first_name,"last_name":body[0].last_name})
          //res.send(body[0])
        } else {
          res.status(404).send('¿ánde andará el usuario?')
        }
      }
  })
})

// Logout con llamada mlabs; nueva versión api para atacar una BD NoSQL (Mongo) a través del API de mlab
app.post('/apitechu/v5/logout', function(req, res) {
  var id = req.headers.id
  var query = 'q={"id":' + id + ', "logged":true}'
  var clienteMlab = requestJson.createClient(urlMlabRaiz + "/usuarios?" + query + "&l=1&" + apiKey) // el apiKey siempre tiene que ir al final ... parece ser que node no entiende qué es si no va al final
  clienteMlab.get('', function(err, resM, body) {
      if (!err) {
        if (body.length == 1) { // Si el usuario está loggeado
          clienteMlab = requestJson.createClient(urlMlabRaiz + "/usuarios")
          var cambio = '{"$set":{"logged":false}}' // actualizar BD
          clienteMlab.put('?q={"id":' + body[0].id + '}&' + apiKey, JSON.parse(cambio), function(errPut, resPut, bodyPut) {
            if (!errPut) {
              res.status(200).send({"logout":"ok", "id":body[0].id})
            } else {
              console.error(errPut.toString())
              res.status(404).send({"Error":"Fatal Error"})
            }
          })
        } else {
          res.status(404).send('Usuario no logado anteriormente')
        }
      }
  })
})

// Llamada mlabs; nueva versión api para atacar una BD NoSQL (Mongo) a través del API de mlab
app.post('/apitechu/v5/login', function(req, res) {
  var username = req.headers.username
  var password = req.headers.password
  var query = 'q={"username":"' + username + '","password":"' + password + '"}'
  console.log(query)

  var clienteMlab = requestJson.createClient(urlMlabRaiz + "/usuarios?" + query + "&l=1&" + apiKey) // el apiKey siempre tiene que ir al final ... parece ser que node no entiende qué es si no va al final
  console.log(urlMlabRaiz + "/usuarios?" + query + "&l=1&" + apiKey)

  clienteMlab.get('', function(err, resM, body) {
      if (!err) {
        if (body.length == 1) { // Login OK
          //http://api.mlab.com/api/1/databases/techuaoa/collections/usuarios?q={"id":2}&apiKey=6huaXLi3YdHe13R9DPEj8zVTwS8h_C7Z
          //var urlMlabRaiz = "http://api.mlab.com/api/1/databases/techuaoa/collections"
          //var apiKey = "apiKey=6huaXLi3YdHe13R9DPEj8zVTwS8h_C7Z"
          clienteMlab = requestJson.createClient(urlMlabRaiz + "/usuarios")
          var cambio = '{"$set":{"logged":true}}' // actualizar BD
          console.log(cambio)

          clienteMlab.put('?q={"id":' + body[0].id + '}&' + apiKey, JSON.parse(cambio), function(errPut, resPut, bodyPut) {
            if (!errPut) {
              console.log("Estoy dentro del if; actualización correcta")
              console.log({"login":"ok", "id":body[0].id, "nombre":body[0].first_name, "apellidos":body[0].last_name})
              res.send({"login":"ok", "id":body[0].id, "nombre":body[0].first_name, "apellidos":body[0].last_name})
            } else {
              //console.log("¿porqué no funciona?" + errPut)
              console.error(errPut.toString())
              res.send({"Error":"Fatal Error"})
            }
          })
        } else {
          res.status(404).send('¿ánde andará el usuario?')
        }
      }
  })
})

// Dado un código de cliente, devuelve el listado de IBANs que tenga el cliente
// Llamada mlabs; nueva versión api para atacar una BD NoSQL (Mongo) a través del API de mlab
app.get('/apitechu/v5/cuentas', function(req, res) {
  var idusuario = req.headers.idusuario
  var query = 'q={"idusuario":' + idusuario + '}&f={"iban":1, "_id":0}&s={"iban":1}'
  console.log(query)

  var clienteMlab = requestJson.createClient(urlMlabRaiz + "/cuentas?" + query + '&' + apiKey) // el apiKey siempre tiene que ir al final ... parece ser que node no entiende qué es si no va al final
  console.log(urlMlabRaiz + "/cuentas?" + query + apiKey)

  clienteMlab.get('', function(err, resM, body) {
      if (!err) { // No hay error, por tanto, tenemos resultados
        res.send(body)
      } else {
        res.status(404).send('El id no tiene cuentas')
      }
  })
})

// Dado un código de cuenta (IBAN), devuelve el listado de movimientos que tenga la cuenta
// Llamada mlabs; nueva versión api para atacar una BD NoSQL (Mongo) a través del API de mlab
app.get('/apitechu/v5/movimientos', function(req, res) {
  var iban = req.headers.iban
  var query = 'q={"iban":' + iban + '}&f={"movimientos":1, "_id":0}&s={"movimientos":1}'
  console.log(query)

  var clienteMlab = requestJson.createClient(urlMlabRaiz + "/cuentas?" + query + '&' + apiKey) // el apiKey siempre tiene que ir al final ... parece ser que node no entiende qué es si no va al final
  console.log(urlMlabRaiz + "/cuentas?" + query + apiKey)

  clienteMlab.get('', function(err, resM, body) {
      if (!err) { // No hay error, por tanto, tenemos resultados
        res.send(body)
      } else {
        res.status(404).send('El id no tiene cuentas')
      }
  })
})

app.post('/apitechu/v1/usuarios', function(req, res)
{
  var nuevo = {"first_name": req.headers.first_name, "country": req.headers.country}
  usuarios.push(nuevo)
  console.log(req.headers)
  const datos = JSON.stringify(usuarios)

  fs.writeFile("./usuarios.json", datos, "utf8", function(err) {
    if (err) {
      console.log(err)
    }
    else {
      console.log("Fichero guardado")
    }
    })
  res.send("ALTA OK")
})

app.delete('/apitechu/v1/usuarios/:id', function(req, res)
{
  usuarios.splice(req.params.id-1, 1)
  res.send("Usuario borrado")
})

app.post('/apitechu/v1/monstruo/:p1/:p2', function(req, res) {
  console.log("Lista de parámetros")
  console.log(req.params)
  console.log("Query strings")
  console.log(req.query)
  console.log("Headers")
  console.log(req.headers)
  console.log("Body")
  console.log(req.body)
})

app.post('/apitechu/v2/usuarios', function(req, res)
{
  //var nuevo = {"first_name": req.headers.first_name, "country": req.headers.country}
  var nuevo = req.body
  usuarios.push(nuevo)
  //console.log(req.headers)
  const datos = JSON.stringify(usuarios)

  fs.writeFile("./usuarios.json", datos, "utf8", function(err) {
    if (err) {
      console.log(err)
    }
    else {
      console.log("Fichero guardado")
    }
    })
  res.send("ALTA OK")
})

app.post('/apitechu/v1/login', function(req, res)
{
  var email = req.headers.email
  var password = req.headers.password
  var idusuario = 0
  var idarray = 0
  console.log("Headers")
  console.log(req.headers)

  for (var i = 0; i < usuarios2.length; i++) {
    if (usuarios2[i].email==email && usuarios2[i].password==password) {
      idusuario=usuarios2[i].id
      usuarios2[i].logged=true
      idarray=i
      break;
    }
  }
  if (idusuario!=0) {
    res.send({"mensaje":"Login successful", "idusuario":idusuario, "idarray":idarray})
  } else {
    res.send({"encontrado":"no"})
  }
})

app.get('/apitechu/v1/usuarios2', function(req, res) {
  res.send(usuarios2)
})

app.post('/apitechu/v1/logout', function(req, res) {
  var idarray = req.headers.idusuario - 1
  if (usuarios2[idarray].logged) {
    usuarios2[req.headers.idusuario].logged = false
    res.send({"mensaje":"Logout successful", "idusuario":req.headers.idusuario})
  } else {
    res.send({"encontrado":"no"})
  }
})

// API REST Cuentas
var cuentas = require('./cuentas.json')

app.get('/apitechu/v1/cuentas', function(req, res) {
  res.status(200).send(cuentas)
})

app.get('/apitechu/v2/cuentas', function(req, res) {
  //console.log(req)
  var listacuentas = []
  for (var i = 0; i < cuentas.length; i++) {
    //listacuentas[i] = cuentas[i].iban
    listacuentas.push(cuentas[i].iban)
  }
  res.send(listacuentas)
})

// v1 Listado de movimientos por IBAN
app.get('/apitechu/v1/movimientos', function(req, res) {
  var movimientos = []
  var encontrado = false
  for (var i = 0; i < cuentas.length; i++) {
    if (cuentas[i].iban == req.headers.iban) {
      movimientos = cuentas[i].movimientos
      i = cuentas.length
      encontrado = true
    }
  }
  if (encontrado) {
    res.send(movimientos)
  } else {
    res.send({"error":"IBAN no encontrado; Hazte cliente de BBVA con un selfie "})
  }
})

// v2 Listado de movimientos por id usuario
app.get('/apitechu/v2/movimientos', function(req, res) {
  // var movimientos = []
  var lCuentasMovimientos = []
  var encontrado = false
  for (var i = 0; i < cuentas.length; i++) {
    if (cuentas[i].idusuario == req.headers.idusuario) {
      // movimientos.push(cuentas[i].movimientos)
      lCuentasMovimientos.push(cuentas[i])
      //lCuentasMovimientos.pop(idusuario)
      //lCuentasMovimientos.pop(saldo)
      // i = cuentas.length
      encontrado = true
    }
  }
  if (encontrado) {
    // res.send(movimientos)
    res.send(lCuentasMovimientos)
  } else {
    res.send({"error":"IBAN no encontrado; Hazte cliente de BBVA con un selfie"})
  }
})

app.get('/apitechu/v2/usuarios/:id', function(req, res) {
  var id = req.params.id
  var query = 'q={"id":' + id + '}&f={"first_name":1, "last_name":1, "_id":0}'
  var clienteMlab = requestJson.createClient(urlMlabRaiz + "/usuarios?" + query + "&" + apiKey) // el apiKey siempre tiene que ir al final ... parece ser que node no entiende qué es si no va al final
  clienteMlab.get('', function(err, resM, body) {
      if (!err) {
        if (body.length == 1) {
          //var user = []
          //user.firstname = {"first_name":body[0].first_name}
          //user.lastname = {"lastname":body[0].last_name}
          res.status(200).send(body)
          //res.send(body[0])
        } else {
          res.status(404).send('¿ánde andará el usuario?')
        }
      }
  })
})
